//package com.covid.covidstatistics.services;
//
//
//import com.covid.covidstatistics.exceptions.DuplicateEntityException;
//import com.covid.covidstatistics.exceptions.EntityNotFoundException;
//import com.covid.covidstatistics.models.Country;
//import com.covid.covidstatistics.repositories.CountryRepository;
//import org.apache.tomcat.jni.Address;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import java.util.ArrayList;
//
//import static com.covid.covidstatistics.services.Helpers.*;
//
//
//@ExtendWith(MockitoExtension.class)
//public class CountryServiceImplTests {
//
//    @Mock
//    CountryRepository mockRepository;
//
//    @InjectMocks
//    CountryServiceImpl service;
//
//    @Test
//    void getAll_should_callRepository() {
//        // Arrange
//        Mockito.when(mockRepository.getAll())
//                .thenReturn(new ArrayList<>());
//
//        // Act
//        service.getAll();
//
//        // Assert
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .getAll();
//    }
//
//    @Test
//    public void getById_should_returnCountry_when_matchExist() {
//        // Arrange
//
//        Country mockCountry = createMockCountry();
//        Mockito.when(mockRepository.getById(mockCountry.getId()))
//                .thenReturn(mockCountry);
//        // Act
//        Country result = service.getById(mockCountry.getId());
//
//
//        // Assert
//        Assertions.assertAll(
//                () -> Assertions.assertEquals(mockCountry.getId(), result.getId()),
//                () -> Assertions.assertEquals(mockCountry.getName(), result.getName())
//        );
//    }
//
//    @Test
//    public void getByCountryCode_should_returnCountry_when_matchExist() {
//        // Arrange
//
//        Country mockCountry = createMockCountry();
//        Mockito.when(mockRepository.getCountryByCode(mockCountry.getCountryCode()))
//                .thenReturn(mockCountry);
//
//        // Act
//        Country result = service.getCountryByCode(mockCountry.getCountryCode());
//
//
//        // Assert
//        Assertions.assertAll(
//                () -> Assertions.assertEquals(mockCountry.getId(), result.getId()),
//                () -> Assertions.assertEquals(mockCountry.getCountryCode(), result.getCountryCode())
//        );
//    }
//
//    @Test
//    public void save_should_throw_when_CountryWithSameNameExists() {
//        // Arrange
//
//        Country mockCountry = createMockCountry();
//
//        Mockito.when(mockRepository.getByName(mockCountry.getName()))
//                .thenReturn(mockCountry);
//
//        // Act, Assert
//        Assertions.assertThrows(DuplicateEntityException.class, () -> service.save(mockCountry));
//    }
//
//
//    @Test
//    public void save_should_callRepository_when_CountryWithSameNameDoesNotExist() {
//        // Arrange
//
//        Country mockCountry = createMockCountry();
//
//        Mockito.when(mockRepository.getByName(mockCountry.getName()))
//                .thenThrow(EntityNotFoundException.class);
//
//        // Act
//        service.save(mockCountry);
//
//        // Assert
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .save(mockCountry);
//    }
//
//}
