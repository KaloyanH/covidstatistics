//package com.covid.covidstatistics.services;
//
//
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//
//import java.util.ArrayList;
//
//import static com.covid.covidstatistics.services.Helpers.createMockStatistic;
//
//@ExtendWith(MockitoExtension.class)
//public class StatisticServiceImplTests {
//
//
//
//    @InjectMocks
//    StatisticServiceImpl service;
//
//    @Test
//    void getAll_should_callRepository() {
//        // Arrange
//        Mockito.when(mockRepository.getAll())
//                .thenReturn(new ArrayList<>());
//
//        // Act
//        service.getAll();
//
//        // Assert
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .getAll();
//    }
//
//    @Test
//    public void getById_should_returnStatistic_when_matchExist() {
//        // Arrange
//
//        Statistic mockStatistic = createMockStatistic();
//        Mockito.when(mockRepository.getById(mockStatistic.getId()))
//                .thenReturn(mockStatistic);
//        // Act
//        Statistic result = service.getById(mockStatistic.getId());
//
//        // Assert
//        Assertions.assertAll(
//                () -> Assertions.assertEquals(mockStatistic.getId(), result.getId())
//        );
//    }
//
//    @Test
//    public void save_should_callRepository() {
//        // Arrange
//
//        Statistic mockStatistic = createMockStatistic();
//
//        Mockito.when(mockRepository.getById(mockStatistic.getId()))
//                .thenReturn(mockStatistic);
//        Statistic result = service.getById(mockStatistic.getId());
//        // Act
//        service.save(result);
//
//        // Assert
//        Mockito.verify(mockRepository, Mockito.times(1))
//                .save(mockStatistic);
//    }
//}
