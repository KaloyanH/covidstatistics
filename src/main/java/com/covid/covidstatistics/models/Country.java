package com.covid.covidstatistics.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "countries")
public class Country {

    @Id
    @JsonProperty("ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "country_id")
    private String id;

    @JsonProperty("Country")
    @Column(name = "name")
    private String name;

    @JsonProperty("CountryCode")
    @Column(name = "country_code")
    private String countryCode;

    @JsonProperty("Slug")
    @Column(name = "slug")
    private String slug;

    @JsonProperty("NewConfirmed")
    @Column(name = "new_confirmed")
    private int newConfirmed;

    @JsonProperty("TotalConfirmed")
    @Column(name = "total_confirmed")
    private int totalConfirmed;

    @JsonProperty("NewDeaths")
    @Column(name = "new_deaths")
    private int newDeaths;

    @JsonProperty("TotalDeaths")
    @Column(name = "total_deaths")
    private int totalDeaths;

    @JsonProperty("NewRecovered")
    @Column(name = "new_recovered")
    private int newRecovered;


    @JsonProperty("TotalRecovered")
    @Column(name = "total_recovered")
    private int totalRecovered;

    @JsonProperty("Date")
    @Column(name = "date")
    private LocalDateTime date;



    public Country() {
    }



    public void setName(String name) {
        this.name = name;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }



    public String getName() {
        return name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getSlug() {
        return slug;
    }

    public int getNewConfirmed() {
        return newConfirmed;
    }

    public void setNewConfirmed(int newConfirmed) {
        this.newConfirmed = newConfirmed;
    }

    public int getTotalConfirmed() {
        return totalConfirmed;
    }

    public void setTotalConfirmed(int totalConfirmed) {
        this.totalConfirmed = totalConfirmed;
    }

    public int getNewDeaths() {
        return newDeaths;
    }

    public void setNewDeaths(int newDeaths) {
        this.newDeaths = newDeaths;
    }

    public int getTotalDeaths() {
        return totalDeaths;
    }

    public void setTotalDeaths(int totalDeaths) {
        this.totalDeaths = totalDeaths;
    }

    public int getNewRecovered() {
        return newRecovered;
    }

    public void setNewRecovered(int newRecovered) {
        this.newRecovered = newRecovered;
    }

    public int getTotalRecovered() {
        return totalRecovered;
    }

    public void setTotalRecovered(int totalRecovered) {
        this.totalRecovered = totalRecovered;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
