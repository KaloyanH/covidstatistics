package com.covid.covidstatistics.models.dto;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class UrlDto {

    @NotNull(message = "Url can't be empty")
    @Size(min = 5, max = 50, message = "Url must be between 5 and 30 symbols.")
    private String theUrl;


    public UrlDto() {
    }

    public String getUrl() {
        return theUrl;
    }

    public void setUrl(String theUrl) {
        this.theUrl = theUrl;
    }
}
