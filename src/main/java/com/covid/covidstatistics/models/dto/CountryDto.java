package com.covid.covidstatistics.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CountryDto {

    @NotNull
    @Size(min = 5, max = 30, message = "Country name must be between 5 and 30 symbols.")
    private String name;

    public CountryDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
