package com.covid.covidstatistics.services;


import com.covid.covidstatistics.exceptions.DuplicateEntityException;
import com.covid.covidstatistics.exceptions.EntityNotFoundException;
import com.covid.covidstatistics.models.Country;
import com.covid.covidstatistics.repositories.CountryRepository;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


import java.io.DataInput;
import java.io.IOException;
import java.time.LocalDateTime;

import java.time.format.DateTimeFormatter;
import java.util.Collections;

import java.util.List;
import java.util.Locale;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;


    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> getAll() {
        return countryRepository.getAll();
    }

    @Override
    public Country getById(int id) {
        Country country = countryRepository.getById(id);
        if (country == null) {
            throw new EntityNotFoundException("Country", id);
        }
        return countryRepository.getById(id);
    }

    @Override
    public void save(Country country) {
        boolean duplicateExists = true;
        try {
            countryRepository.getByName(country.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Country", "name", country.getName());
        }

        countryRepository.save(country);
    }

    @Override
    public Country getCountryByCode(String countryCode) {

        if (countryRepository.getCountryByCode(countryCode) == null) {
            throw new EntityNotFoundException("Country", countryCode, countryCode);
        }
        return countryRepository.getCountryByCode(countryCode);
    }

    @Override
    public Country getCountryByName(String countryName) {
        return countryRepository.getByName(countryName);
    }

    @Override
    public void getUrlContent(String url) throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        String fooResourceUrl
                = url;
        ResponseEntity<String> response
                = restTemplate.getForEntity(fooResourceUrl, String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(response.getBody());
        List<JsonNode> countries = Collections.singletonList(root.findValue("Countries"));
//        Country country = mapper.readValue(response.getBody(). Country.class);
        for (int i = 0; i < countries.get(0).size(); i++) {
            Country country = mapper.convertValue(countries.get(0).get(i), Country.class);
        }
//        countryRepository.save( );
    }


}
