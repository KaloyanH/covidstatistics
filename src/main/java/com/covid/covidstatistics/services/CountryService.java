package com.covid.covidstatistics.services;


import com.covid.covidstatistics.models.Country;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.text.ParseException;
import java.util.List;


public interface CountryService {

    List<Country> getAll();

    Country getById(int id);

    void save(Country country);

    Country getCountryByCode(String countryCode);

    Country getCountryByName(String countryName);

    void getUrlContent(String s) throws JsonProcessingException, ParseException;
}
