package com.covid.covidstatistics.controllers.rest;


import com.covid.covidstatistics.exceptions.CapitalLettersException;
import com.covid.covidstatistics.exceptions.EntityNotFoundException;
import com.covid.covidstatistics.models.Country;
import com.covid.covidstatistics.services.CountryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


import java.text.ParseException;
import java.util.*;


@RestController
@RequestMapping("api/countries")
public class CountryController extends TimerTask {

    private final CountryService countryService;

    @Autowired
    public CountryController(CountryService countryService)  {
        this.countryService = countryService;

    }

    @GetMapping
    public List<Country> getAll() throws ParseException, JsonProcessingException {
        countryService.getUrlContent("https://api.covid19api.com/summary");
        return countryService.getAll();

    }

    @GetMapping("/{id}")
    public Country getById(@PathVariable int id) {
        try {
            return countryService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());

        }
    }

    @GetMapping("/country/{countryCode}")
    public Country getCountryByCode(@PathVariable String countryCode) {
        char firsLetter = countryCode.charAt(0);
        char secondLetter = countryCode.charAt(1);
        try {
            if (!Character.isUpperCase(firsLetter) || !Character.isUpperCase(secondLetter)) {
                throw new CapitalLettersException(countryCode);
            }
            return countryService.getCountryByCode(countryCode);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("countryName/{countryName}")
    public Country getCountryByName(@PathVariable String countryName) {

        try {
            return countryService.getCountryByName(countryName);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @Override
    public void run() {
        Timer timer = new Timer();

    }
}
