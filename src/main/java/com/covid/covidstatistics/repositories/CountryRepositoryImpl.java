package com.covid.covidstatistics.repositories;

import com.covid.covidstatistics.exceptions.EntityNotFoundException;
import com.covid.covidstatistics.models.Country;
import com.fasterxml.jackson.databind.JsonNode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryRepositoryImpl implements CountryRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Country> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country", Country.class);
            return query.list();
        }
    }

    @Override
    public Country getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Country country = session.get(Country.class, id);
            if (country == null) {
                throw new EntityNotFoundException("Country", id);
            }
            return country;
        }
    }

    @Override
    public Country getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country a where a.name = :name", Country.class);
            query.setParameter("name", name);

            List<Country> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Country", "name", name);
            }
            return result.get(0);
        }
    }

    @Override
    public Country getCountryByCode(String countryCode) {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country a where a.countryCode = :countryCode", Country.class);
            query.setParameter("countryCode", countryCode);

            List<Country> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Country", "countryCode", countryCode);
            }
            return result.get(0);
        }
    }

    @Override
    public void save(Country country) {
        try (Session session = sessionFactory.openSession()) {
            session.save(country);
        }
    }
}
