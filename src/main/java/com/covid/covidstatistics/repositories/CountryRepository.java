package com.covid.covidstatistics.repositories;


import com.covid.covidstatistics.models.Country;
import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public interface CountryRepository {

    List<Country> getAll();

    Country getById(int id);

    Country getByName(String name);

    Country getCountryByCode(String countryCode);

    void save(Country country);
}
