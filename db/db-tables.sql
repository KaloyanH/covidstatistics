DROP DATABASE IF EXISTS covidstatistics;
CREATE DATABASE IF NOT EXISTS covidstatistics;
USE covidstatistics;

create or replace table countries
(
    country_id int auto_increment
        primary key,
    name varchar(50) null,
    country_code varchar(10) null,
    slug varchar(50) null,
    new_confirmed bigint null,
    total_confirmed bigint null,
    new_deaths bigint null,
    total_deaths bigint null,
    new_recovered bigint null,
    total_recovered bigint null,
    date datetime null
);

